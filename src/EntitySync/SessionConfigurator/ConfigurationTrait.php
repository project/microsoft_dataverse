<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\EntitySync\SessionConfigurator;

/**
 * Provides methods related to configuring OAuth2 session configurator plugins.
 */
trait ConfigurationTrait {

  /**
   * Returns the default authentication configuration.
   *
   * @return array
   *   The configuration array.
   */
  public function defaultAuthenticationConfiguration() {
    return [
      'authentication' => [
        'url_authorize' => 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        'url_access_token' => 'https://login.microsoftonline.com/70b43e6b-60cd-4609-a5fa-3bd59502c6a3/oauth2/v2.0/token',
        'url_resource_owner_details' => 'https://graph.microsoft.com/oidc/userinfo',
      ],
    ];
  }

}
