<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\Plugin\EntitySync\SessionConfigurator;

use Drupal\entity_sync_session\SessionConfigurator\Oauth2\PasswordBase;
use Drupal\microsoft_dataverse\EntitySync\SessionConfigurator\ConfigurationTrait;

/**
 * Session configurator for using OAuth2 resource owner password credentials.
 *
 * phpcs:disable
 * @EntitySyncSessionConfigurator(
 *   id = "microsoft_dataverse_oauth2_password",
 *   label = @Translation("Microsoft Dataverse OAuth2 password"),
 *   description = @Translation(
 *     "Session configurator for connecting to Microsoft Dataverse using OAuth2 resource owner password credentials."
 *   ),
 * )
 * phpcs:enable
 */
class Oauth2Password extends PasswordBase {

  use ConfigurationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->defaultAuthenticationConfiguration() +
      parent::defaultConfiguration();
  }

}
