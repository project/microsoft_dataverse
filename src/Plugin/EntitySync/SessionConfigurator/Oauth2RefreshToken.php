<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\Plugin\EntitySync\SessionConfigurator;

use Drupal\entity_sync_session\SessionConfigurator\Oauth2\RefreshTokenBase;
use Drupal\microsoft_dataverse\EntitySync\SessionConfigurator\ConfigurationTrait;

/**
 * Session configurator for using OAuth2 refresh token.
 *
 * phpcs:disable
 * @EntitySyncSessionConfigurator(
 *   id = "microsoft_dataverse_oauth2_refresh_token",
 *   label = @Translation("Microsoft Dataverse OAuth2 refresh token"),
 *   description = @Translation(
 *     "Session configurator for connecting to Microsoft Dataverse using OAuth2 token refresh."
 *   ),
 * )
 * phpcs:enable
 */
class Oauth2RefreshToken extends RefreshTokenBase {

  use ConfigurationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->defaultAuthenticationConfiguration() +
      parent::defaultConfiguration();
  }

}
