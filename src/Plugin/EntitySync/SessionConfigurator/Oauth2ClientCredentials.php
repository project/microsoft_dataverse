<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\Plugin\EntitySync\SessionConfigurator;

use Drupal\entity_sync_session\SessionConfigurator\Oauth2\ClientCredentialsBase;
use Drupal\microsoft_dataverse\EntitySync\SessionConfigurator\ConfigurationTrait;

/**
 * Session configurator for using OAuth2 client credentials.
 *
 * phpcs:disable
 * @EntitySyncSessionConfigurator(
 *   id = "microsoft_dataverse_oauth2_client_credentials",
 *   label = @Translation("Microsoft Dataverse OAuth2 client credentials"),
 *   description = @Translation(
 *     "Session configurator for connecting to Microsoft Dataverse using OAuth2 client credentials."
 *   ),
 * )
 * phpcs:enable
 */
class Oauth2ClientCredentials extends ClientCredentialsBase {

  use ConfigurationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->defaultAuthenticationConfiguration() +
      parent::defaultConfiguration();
  }

}
