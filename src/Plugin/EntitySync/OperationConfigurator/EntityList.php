<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\DefaultPluginBase;

/**
 * Plugin for operations that act on lists of entities.
 *
 * That is, for operations synchronizing lists of entities between Drupal and
 * Microsoft Dataverse. Currently only imports are supported. Support for
 * exports should be added in this plugin.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "microsoft_dataverse_entity_list",
 *   label = @Translation("Microsoft Dataverse entity list import"),
 *   description = @Translation(
 *     "Use 'Microsoft Dataverse list import' types to import a list of Dataverse tables into Drupal entities."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class EntityList extends DefaultPluginBase {

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'entity_sync.runner.entity_import_list';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'id_field' => NULL,
        'client' => [
          'type' => 'factory',
          'service' => 'entity_sync_odata.entity_sync_client.entity_factory',
          'parameters' => [],
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

}
