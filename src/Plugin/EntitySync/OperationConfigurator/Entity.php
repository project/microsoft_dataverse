<?php

declare(strict_types=1);

namespace Drupal\microsoft_dataverse\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\DefaultPluginBase;
use Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait;

/**
 * Plugin for operations that act on individual entities.
 *
 * That is, for operations synchronizing individual entities between Drupal and
 * Microsoft Dataverse. Currently only exports are supported. Support for
 * imports should be added in this plugin.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "microsoft_dataverse_entity",
 *   label = @Translation("Microsoft Dataverse entity export"),
 *   description = @Translation(
 *     "Use 'Microsoft Dataverse entity import' types to export Drupal entities into Dataverse table rows."
 *   ),
 *   action_types = {
 *     "export_entity",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class Entity extends DefaultPluginBase {

  use EntityTrait;

  /**
   * The ID of the operation runner service.
   */
  const RUNNER_SERVICE_ID = 'entity_sync.runner.entity_export';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action_type' => NULL,
      'local_entity' => [],
      'remote_resource' => [
        'id_field' => NULL,
        'client' => [
          'type' => 'factory',
          'service' => 'entity_sync_odata.entity_sync_client.entity_factory',
          'parameters' => [],
        ],
      ],
      'operations' => [],
      'field_mapping' => [],
      'ui' => [
        'disallowed_transitions' => [
          'fail',
          'complete',
        ],
        'uneditable_states' => [
          'cancelled',
          'running',
          'completed',
          'failed',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();

    // Add the dynamic entity reference field.
    $fields += $this->buildEntityBundleFieldDefinitions();

    return $fields;
  }

}
